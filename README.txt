Esse é o comando que utilizei para deixar o acesso somente via ssl para o banco de dados 
(todo o tráfego local é feito via SSL com o banco de dados):

UPDATE mysql.user SET ssl_type = 'any' WHERE ssl_type = '';
FLUSH PRIVILEGES;

mysql> status;
--------------
mysql  Ver 8.0.19 for Win64 on x86_64 (MySQL Community Server - GPL)

Connection id:          30
Current database:       financeiro
Current user:           root@localhost
SSL:                    Cipher in use is TLS_AES_256_GCM_SHA384
Using delimiter:        ;
Server version:         8.0.19 MySQL Community Server - GPL
Protocol version:       10
Connection:             localhost via TCP/IP
Server characterset:    utf8mb4
Db     characterset:    utf8
Client characterset:    cp850
Conn.  characterset:    cp850
TCP port:               3306
Binary data as:         Hexadecimal
Uptime:                 37 min 43 sec

Threads: 6  Questions: 880  Slow queries: 0  Opens: 302  Flush tables: 3  Open tables: 222  Queries per second avg: 0.388
-------------------------------------------------------------------------------------------------------------------------
A chave do https esetá em src/main/resources, a localhost.jks.
Senha: password
-------------------------------------------------------------------------------------------------------------------------
No banco de dados local, as informações de acesso foram:
usuário: root
senha: password
------------------------------------------------------------------------------------------------------------------------
As informações de acesso e as configurações gerais estão todos no arquivo application.properties e são carregadas
automaticamente pelo sistema
------------------------------------------------------------------------------------------------------------------------
Um banco de dados do tipo mock pode ser criado apenas retirando a anotação @Disabled do método incluirBaseA(), localizado
dentro no caminho /financeiro/src/test/java/br/pr/ct/financeiro/FinanceiroApplicationTests.java
