package br.pr.ct.financeiro.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.basea.service.EntidadeBaseAService;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/financeiro/api/v1/basea")
public class EntidadeBaseAController {

	@Autowired
	private EntidadeBaseAService service;
	
	@Autowired
	private MensagemHelper mensagem;
	
	@GetMapping
	public ResponseEntity<Iterable<EntidadeBaseADTO>> get(
			@RequestParam(name = "page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "10") int size
		) {
		final Iterable<EntidadeBaseADTO> entidades = this.service.getAll(PageRequest.of(page, size));
		return ResponseEntity.ok(entidades);
	}
	
	@ResponseBody
	@PutMapping("/{id}")
	public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody EntidadeBaseADTO base) {
		this.service.update(id, EntidadeBaseA.create(base));
		log.info(this.mensagem.objetoAtualizado);
		return ResponseEntity.ok(this.mensagem.objetoAtualizado);
	}
	
	@ResponseBody
	@PostMapping("")
	public ResponseEntity<EntidadeBaseADTO> inserir(@RequestBody EntidadeBaseADTO base) {
		base = this.service.inserir(EntidadeBaseA.create(base));
		log.info(this.mensagem.objetoInserido);
		return ResponseEntity.ok(base);
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public ResponseEntity<EntidadeBaseADTO> getById(@PathVariable(name = "id") Long id) {
		final EntidadeBaseADTO base = this.service.getById(id);
		return ResponseEntity.ok(base);
	}
	
	@ResponseBody
	@GetMapping("/find")
	public ResponseEntity<EntidadeBaseADTO> getByCpf(@RequestParam(name = "cpf", required = true) String cpf) {
		final EntidadeBaseADTO base = this.service.getByCpf(cpf);
		return ResponseEntity.ok(base);
	}
}
