package br.pr.ct.financeiro.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.pr.ct.financeiro.domain.financeiro.dto.FinanceiroRequestDTO;
import br.pr.ct.financeiro.domain.financeiro.dto.FinanceiroResponseDTO;
import br.pr.ct.financeiro.domain.financeiro.service.FinanceiroService;
import br.pr.ct.financeiro.domain.helper.CpfConverter;

@RestController
@RequestMapping("/financeiro/api/v1/limite")
public class FinanceiroController {

	@Autowired
	private FinanceiroService service;
	
	@GetMapping
	public ResponseEntity<FinanceiroResponseDTO> consultarLimite(
		@RequestParam(name = "cpf", required = true) String cpf, 
		@RequestParam(name = "credito", defaultValue = "0") Double credito) {
		final FinanceiroRequestDTO request = new FinanceiroRequestDTO(CpfConverter.replace(cpf), credito);
		final FinanceiroResponseDTO response = this.service.creditoLimite(request);
		return ResponseEntity.ok(response);
	}
}
