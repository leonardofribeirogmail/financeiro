package br.pr.ct.financeiro.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pr.ct.financeiro.domain.helper.MensagemHelper;

@RestController
@RequestMapping("/")
public class IndexController {
	
	@Autowired
	private MensagemHelper mensagem;

	@GetMapping
	public String get() {return this.mensagem.financeiro;}
	
	@GetMapping("financeiro")
	public String getDefault() {return this.mensagem.financeiro;}
}
