package br.pr.ct.financeiro.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.pr.ct.financeiro.domain.baseb.dto.EntidadeBaseBDTO;
import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import br.pr.ct.financeiro.domain.baseb.service.EntidadeBaseBService;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/financeiro/api/v1/baseb")
public class EntidadeBaseBController {

	@Autowired
	private EntidadeBaseBService service;
	
	@Autowired
	private MensagemHelper mensagem;
	
	@GetMapping
	public ResponseEntity<Iterable<EntidadeBaseBDTO>> get(
			@RequestParam(name = "page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "10") int size
		) {
		final Iterable<EntidadeBaseBDTO> entidades = this.service.getAll(PageRequest.of(page, size));
		return ResponseEntity.ok(entidades);
	}
	
	@ResponseBody
	@PutMapping("/{id}")
	public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody EntidadeBaseBDTO base) {
		this.service.update(id, EntidadeBaseB.create(base));
		log.info(this.mensagem.objetoAtualizado);
		return ResponseEntity.ok(this.mensagem.objetoAtualizado);
	}
	
	@ResponseBody
	@PostMapping("")
	public ResponseEntity<EntidadeBaseBDTO> inserir(@RequestBody EntidadeBaseBDTO base) {
		base = this.service.inserir(EntidadeBaseB.create(base));
		log.info(this.mensagem.objetoInserido);
		return ResponseEntity.ok(base);
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public ResponseEntity<EntidadeBaseBDTO> getById(@PathVariable(name = "id") Long id) {
		final EntidadeBaseBDTO base = this.service.getById(id);
		return ResponseEntity.ok(base);
	}
}
