package br.pr.ct.financeiro.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.pr.ct.financeiro.domain.basec.dto.EntidadeBaseCDTO;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import br.pr.ct.financeiro.domain.basec.service.EntidadeBaseCService;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/financeiro/api/v1/basec")
public class EntidadeBaseCController {

	@Autowired
	private EntidadeBaseCService service;
	
	@Autowired
	private MensagemHelper mensagem;
	
	@GetMapping
	public ResponseEntity<Iterable<EntidadeBaseCDTO>> get(
			@RequestParam(name = "page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "10") int size
		) {
		final Iterable<EntidadeBaseCDTO> entidades = this.service.getAll(PageRequest.of(page, size));
		return ResponseEntity.ok(entidades);
	}
	
	@ResponseBody
	@PutMapping("/{id}")
	public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody EntidadeBaseCDTO base) {
		this.service.update(id, EntidadeBaseC.create(base));
		log.info(this.mensagem.objetoAtualizado);
		return ResponseEntity.ok(this.mensagem.objetoAtualizado);
	}
	
	@ResponseBody
	@PostMapping("")
	public ResponseEntity<EntidadeBaseCDTO> inserir(@RequestBody EntidadeBaseCDTO base) {
		base = this.service.inserir(EntidadeBaseC.create(base));
		log.info(this.mensagem.objetoInserido);
		return ResponseEntity.ok(base);
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public ResponseEntity<EntidadeBaseCDTO> getById(@PathVariable(name = "id") Long id) {
		final EntidadeBaseCDTO base = this.service.getById(id);
		return ResponseEntity.ok(base);
	}
	
	@ResponseBody
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable(name = "id", required = true) Long id) {
		this.service.delete(id);
		return ResponseEntity.ok(this.mensagem.objetoDeletado);
	}
}
