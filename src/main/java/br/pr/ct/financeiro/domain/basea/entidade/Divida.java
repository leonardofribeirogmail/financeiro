package br.pr.ct.financeiro.domain.basea.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.pr.ct.financeiro.domain.helper.security.DoubleSecuredConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity(name = "divida")
@NoArgsConstructor @AllArgsConstructor
public class Divida implements Serializable {

	private static final long serialVersionUID = 6865797423178273863L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "vr_divida")
	@Convert(converter = DoubleSecuredConverter.class)
	private Double vrDivida;
}
