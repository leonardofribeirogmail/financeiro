package br.pr.ct.financeiro.domain.helper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.beans.factory.annotation.Value;

import br.pr.ct.financeiro.domain.exception.CartaoInvalidoException;

@Converter
public class CartaoConverter implements AttributeConverter<String, String> {
	
	private Integer lengthPadrao = 16;
	
	@Value("${app.custom.exception.cartao.invalido}")
	private String mensagemErro;

	@Override
	public String convertToDatabaseColumn(String attribute) {
		if(attribute == null || attribute.isEmpty()) {
			return null;
		}
		
		String novoAtributo = attribute.trim().replaceAll("\\.", "").replaceAll("\\-", "");
		if(novoAtributo.length() != this.lengthPadrao) {
			throw new CartaoInvalidoException(this.mensagemErro);
		}
		
		if(!novoAtributo.contains("*")) {
			attribute = converterParaAsterisco(novoAtributo);
		}
		
		return attribute;
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		return dbData;
	}
	
	private String converterParaAsterisco(final String attribute) {
		final StringBuilder builder = new StringBuilder();
		builder.append(attribute.substring(0, 6));
		builder.append("******");
		builder.append(attribute.substring(12, 16));
		return builder.toString();
	}
}
