package br.pr.ct.financeiro.domain.helper.security;

import javax.persistence.AttributeConverter;

import org.springframework.beans.factory.annotation.Autowired;

public class StringSecuredConverter implements AttributeConverter<String, String> {
	
	@Autowired
	private SecureConverter secure;

	@Override
	public String convertToDatabaseColumn(String attribute) {
		if(attribute == null || attribute.isEmpty()) {
			return null;
		}
		return this.secure.convertToDatabaseColumn(attribute);
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		if(dbData == null || dbData.isEmpty()) {
			return "";
		}
		return this.secure.convertToEntityAttribute(dbData);
	}
}
