package br.pr.ct.financeiro.domain.basea.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;

public interface EntidadeBaseARepository extends JpaRepository<EntidadeBaseA, Long> {

	Optional<EntidadeBaseA> findByCpf(final String cpf);
}
