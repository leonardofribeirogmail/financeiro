package br.pr.ct.financeiro.domain.learning.util;

import java.util.Date;


public class Timer{
	
	private Date start;

	/**
	 * Inicia contagem
	 */
	public Timer(){
		reset();
	}
	
	
	/**
	 *  Tempo decorrido após  inicio da medição 
	 *  
	 * @return
	 */
	public long getTime(){
		Date now = new Date();
		return now.getTime() - this.start.getTime();
	}
	
	
	public void reset(){
		start = new Date(); 
	}
	
	/**
	 * Formata tempo decorrido após  inicio da medição para o formato 0:00.000
	 * 
	 * @param mili
	 * @return
	 */
	public String toString( boolean mili ){
		long millis = getTime();
		long hours = millis / 1000 / 60 / 60;
		millis -= hours * 1000 * 60 * 60;
		long minutes = millis / 1000 / 60;
		millis -= minutes * 1000 * 60;
		long seconds = millis / 1000;
		millis -= seconds * 1000;
		StringBuilder time = new StringBuilder();
		if( hours > 0 )
			time.append( hours + ":" );
		if( hours > 0 && minutes < 10 )
			time.append( "0" );
		time.append( minutes + ":" );
		if( seconds < 10 )
			time.append( "0" );
		time.append( seconds );
		if( mili ){
			time.append( "." );
			if( millis < 100 )
				time.append( "0" );
			if( millis < 10 )
				time.append( "0" );
			time.append( millis );
		}
		return time.toString();
	}
	
	@Override
	public String toString(){
		return toString(true);
	}
}
