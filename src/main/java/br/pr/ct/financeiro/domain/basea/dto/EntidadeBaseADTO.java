package br.pr.ct.financeiro.domain.basea.dto;

import java.io.Serializable;
import java.util.List;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import lombok.Data;

@Data
public class EntidadeBaseADTO implements Serializable {

	private static final long serialVersionUID = 6364337870459208924L;

	private Long id;
	private String cpf;
	private String nome;
	private Double renda;
	private String endereco;
	private List<DividaDTO> dividas;
	
	public static EntidadeBaseADTO create(Long id) {
		final EntidadeBaseADTO base = new EntidadeBaseADTO();
		base.id = id;
		return base;
	}
	
	public static EntidadeBaseADTO create(EntidadeBaseA base) {
		final ModelMapper mapper = new ModelMapper();
		return mapper.map(base, EntidadeBaseADTO.class);
	}
}
