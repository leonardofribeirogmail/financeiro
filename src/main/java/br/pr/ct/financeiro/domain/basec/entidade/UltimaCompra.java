package br.pr.ct.financeiro.domain.basec.entidade;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pr.ct.financeiro.domain.helper.CartaoConverter;
import lombok.Data;

@Data
@Entity
@Table(name = "ultimacompra", indexes = {@Index(columnList = "cartao, vr_compra, dt_compra")})
public class UltimaCompra implements Serializable {

	private static final long serialVersionUID = -7824633252092901750L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 20, nullable = false)
	@Convert(converter = CartaoConverter.class)
	private String cartao;
	
	@Column(name = "vr_compra", nullable = false)
	private Double vrCompra;
	
	@Type(type = "calendar")
	@Column(name = "dt_compra", nullable = false)
	private Calendar dtCompra;
}
