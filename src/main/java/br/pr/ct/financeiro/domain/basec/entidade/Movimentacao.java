package br.pr.ct.financeiro.domain.basec.entidade;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;

@Data
@Entity
@Table(name = "movimentacao", indexes = {@Index(columnList = "vr_movimento, dt_movimento")})
public class Movimentacao implements Serializable {

	private static final long serialVersionUID = -613294199690259293L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "vr_movimento")
	private Double vrMovimento;
	
	@Column(name = "dt_movimento") @Type(type = "calendar")
	private Calendar dtMovimento;
}
