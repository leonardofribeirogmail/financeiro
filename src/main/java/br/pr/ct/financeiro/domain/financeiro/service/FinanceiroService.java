package br.pr.ct.financeiro.domain.financeiro.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import br.pr.ct.financeiro.domain.financeiro.dto.FinanceiroRequestDTO;
import br.pr.ct.financeiro.domain.financeiro.dto.FinanceiroResponseDTO;
import br.pr.ct.financeiro.domain.helper.CpfConverter;
import br.pr.ct.financeiro.domain.helper.FinanceiroCalculo;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;

@Service
public class FinanceiroService {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	private EntityManager manager;
	
	@Autowired
	private FinanceiroCalculo calculo;
	
	@Autowired
	private MensagemHelper mensagem;
	
	@Transactional
	public FinanceiroResponseDTO creditoLimite(final FinanceiroRequestDTO request) {
		Assert.notNull(request.getCpf(), this.mensagem.objetoInvalido);
		Assert.notNull(request.getCredito(), this.mensagem.objetoInvalido);
		final FinanceiroResponseDTO response = new FinanceiroResponseDTO();
		
		String query = "select baseB from entidadebaseb as baseB join baseB.base as baseA ";
		query += "where baseA.cpf = '"+request.getCpf()+"'";
		
		final TypedQuery<EntidadeBaseB> typedQueryB = this.manager.createQuery(query, EntidadeBaseB.class);
		typedQueryB.setMaxResults(1);
		
		EntidadeBaseB baseB = typedQueryB.getSingleResult();
		Assert.notNull(baseB, this.mensagem.objetoNaoEncontrado);
		
		query = query.replace("baseB", "baseC").replace("entidadebaseb", "entidadebasec");
		
		final TypedQuery<EntidadeBaseC> typedQueryC = this.manager.createQuery(query, EntidadeBaseC.class);
		typedQueryC.setMaxResults(1);
		
		EntidadeBaseC baseC = typedQueryC.getSingleResult();
		
		final Double limite = this.calculo.calcularLimiteDeCredito(request.getCredito(), baseB, baseC);
		response.setCpf(CpfConverter.deplace(request.getCpf()));
		response.setLimite(limite);
		return response;
	}
}
