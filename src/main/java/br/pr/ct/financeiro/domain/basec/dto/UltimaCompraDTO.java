package br.pr.ct.financeiro.domain.basec.dto;

import java.io.Serializable;
import java.util.Calendar;

import lombok.Data;

@Data
public class UltimaCompraDTO implements Serializable {

	private static final long serialVersionUID = 4308070776808302630L;

	private Long id;
	private String cartao;
	private Double vrCompra;
	private Calendar dtCompra;
}
