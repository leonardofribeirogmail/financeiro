package br.pr.ct.financeiro.domain.baseb.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ClienteBemDTO implements Serializable {

	private static final long serialVersionUID = -8414161644916826724L;

	private Long id;
	private String tipo;
	private Double vrBem;
	private String descricao;
}
