package br.pr.ct.financeiro.domain.helper.security;

import javax.persistence.AttributeConverter;

import org.springframework.beans.factory.annotation.Autowired;

public class DoubleSecuredConverter implements AttributeConverter<Double, String> {
	
	@Autowired
	private SecureConverter secure;
	
	@Override
	public String convertToDatabaseColumn(Double attribute) {
		if(attribute == null) {
			return null;
		}
		return this.secure.convertToDatabaseColumn(Double.toString(attribute));
	}
	@Override
	public Double convertToEntityAttribute(String dbData) {
		if(dbData == null || dbData.isEmpty()) {
			return null;
		}
		return Double.valueOf(this.secure.convertToEntityAttribute(dbData));
	}
}
