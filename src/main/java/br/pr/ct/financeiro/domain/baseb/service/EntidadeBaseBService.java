package br.pr.ct.financeiro.domain.baseb.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.baseb.dto.EntidadeBaseBDTO;
import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import br.pr.ct.financeiro.domain.baseb.repository.EntidadeBaseBRepository;
import br.pr.ct.financeiro.domain.exception.BancoDadosException;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;

@Service
public class EntidadeBaseBService {

	@Autowired
	private MensagemHelper mensagem;
	
	@Autowired
	private EntidadeBaseBRepository repository;
	
	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	private EntityManager manager;
	
	public List<EntidadeBaseBDTO> getAll(Pageable pageable) {
		return this.repository.findAll(pageable).stream().map(EntidadeBaseBDTO::create).collect(Collectors.toList());
	}
	
	@Transactional
	public EntidadeBaseBDTO inserir(EntidadeBaseB base) {
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		Assert.notNull(base.getBase(), this.mensagem.objetoInvalido);
		Assert.notNull(base.getBase().getId(), this.mensagem.objetoInvalido);
		EntidadeBaseA baseA = this.manager.find(EntidadeBaseA.class, base.getBase().getId());
		base.setBase(baseA);
		base = this.manager.merge(base);
		return EntidadeBaseBDTO.create(base);
	}
	
	public EntidadeBaseBDTO update(Long id, EntidadeBaseB base) {
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		final Optional<EntidadeBaseB> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			EntidadeBaseB db = optional.get();
			db.setBens(base.getBens());
			db.setEndereco(base.getEndereco());
			db.setFonteRenda(base.getFonteRenda());
			db.setIdade(base.getIdade());
			db = this.repository.save(db);
			return EntidadeBaseBDTO.create(db);
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	}
	
	public EntidadeBaseBDTO getById(Long id) {
		Optional<EntidadeBaseB> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			final EntidadeBaseB base = optional.get();
			return EntidadeBaseBDTO.create(base);
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	}
}
