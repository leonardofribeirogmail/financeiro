package br.pr.ct.financeiro.domain.basec.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import br.pr.ct.financeiro.domain.helper.DataConverter;
import lombok.Data;

@Data
@Entity
@Table(name = "bureau", indexes = {@Index(columnList = "dt_consulta, nome_bureau")})
public class Bureau implements Serializable {

	private static final long serialVersionUID = 5900227749917502723L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "dt_consulta")
	@Convert(converter = DataConverter.class)
	private Date dtConsulta;
	
	@Column(name = "nome_bureau")
	private String nomeBureau;
}
