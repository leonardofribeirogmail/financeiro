package br.pr.ct.financeiro.domain.exception;

public class SecretConversionException extends RuntimeException {

	private static final long serialVersionUID = -6483769204226579371L;

	public SecretConversionException(final String message) {
		super(message);
	}
	
	public SecretConversionException(final Throwable e) {
		super(e);
	}
	
	public SecretConversionException(final String message, final Throwable e) {
		super(message, e);
	}
}
