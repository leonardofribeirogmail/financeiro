package br.pr.ct.financeiro.domain.basea.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.helper.CpfConverter;
import br.pr.ct.financeiro.domain.helper.security.DoubleSecuredConverter;
import br.pr.ct.financeiro.domain.helper.security.StringSecuredConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity(name = "entidadebasea")
@NoArgsConstructor @AllArgsConstructor
public class EntidadeBaseA implements Serializable {

	private static final long serialVersionUID = -120100850972518849L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 11, unique = true, updatable = false)
	@Convert(converter = CpfConverter.class)
	private String cpf;
	
	@Column 
	@Convert(converter = StringSecuredConverter.class)
	private String nome;
	
	@Column
	@Convert(converter = StringSecuredConverter.class)
	private String endereco;
	
	@Column(nullable = false)
	@Convert(converter = DoubleSecuredConverter.class)
	private Double renda;
	
	@JoinColumn(name = "id_entidadebase_a")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Divida> dividas;
	
	public static EntidadeBaseA create(EntidadeBaseADTO base) {
		final ModelMapper mapper = new ModelMapper();
		return mapper.map(base, EntidadeBaseA.class);
	}
}
