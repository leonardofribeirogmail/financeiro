package br.pr.ct.financeiro.domain.basec.dto;

import java.io.Serializable;
import java.util.Calendar;

import lombok.Data;

@Data
public class MovimentacaoDTO implements Serializable {

	private static final long serialVersionUID = -1440192024529914903L;

	private Long id;
	private Double vrMovimento;
	private Calendar dtMovimento;
}
