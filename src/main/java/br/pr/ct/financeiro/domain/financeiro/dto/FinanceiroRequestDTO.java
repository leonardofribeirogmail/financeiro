package br.pr.ct.financeiro.domain.financeiro.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class FinanceiroRequestDTO implements Serializable {

	private static final long serialVersionUID = -2657713939067080963L;
	
	private String cpf;
	private Double credito;
}
