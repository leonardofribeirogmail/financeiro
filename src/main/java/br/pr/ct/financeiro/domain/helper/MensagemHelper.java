package br.pr.ct.financeiro.domain.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MensagemHelper {
	
	@Value("${app.custom.financeiro}")
	public String financeiro;

	@Value("${app.custom.mensagem.objeto.nao.encontrado}")
	public String objetoNaoEncontrado;
	
	@Value("${app.custom.objeto.atualizado}")
	public String objetoAtualizado;
	
	@Value("${app.custom.objeto.inserido}")
	public String objetoInserido;
	
	@Value("${app.custom.objeto.invalido}")
	public String objetoInvalido;
	
	@Value("${app.custom.objeto.deletado}")
	public String objetoDeletado;
}
