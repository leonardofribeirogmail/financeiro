package br.pr.ct.financeiro.domain.learning.model;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import br.pr.ct.financeiro.domain.learning.util.ConfigParameters;
import br.pr.ct.financeiro.domain.learning.util.ProductCommonProcessor;
import br.pr.ct.financeiro.domain.learning.util.Timer;


/**
 * Classe respons�vel pelo treinamento do modelo
 * @author 645547
 *
 */
public class DeepModelUtils {
			
	private String modelPath = null;
	private String modelName = null;
	private String trainindData = null;
	private String trainingEpochs = null;
	private static final Logger LOGGER = LogManager.getLogger(DeepModelUtils.class);
	
	/**
	 * Inicia classe responsavel pela geracao do modelo
	 * 
	 * @param pathModel : Caminho referente ao local onde o modelo sera salvo
	 * @param nameModel : Nome do arquivo de modelo
	 * @param trnData : Caminho para local onde se localizam os arquivos de treinamento
	 * @param nbEpochs : Numero de epocas de treinamento
	 * 
	 * @throws Exception
	 */
	public DeepModelUtils(String pathModel ,String nameModel ,String trnData ,String nbEpochs ) {
		
		/**
		 * Se um dos parametros nao forem enviados, considero os valores default disponiveis no 
		 * Enum contendo par�metros de configuracao
		 */
		
		try {
			if(pathModel == null || nameModel == null || trnData == null || nbEpochs == null ) {
				this.modelPath = ConfigParameters.MODEL_PATH.getValor();
				this.modelName = ConfigParameters.MODEL_NAME.getValor();
				this.trainindData = ConfigParameters.TRAIN_DATA_PATH.getValor();
				this.trainingEpochs = String.valueOf(100);
			}else {
				this.modelPath = pathModel;
				this.modelName = nameModel;
				this.trainindData = trnData;
				this.trainingEpochs = nbEpochs;
			}
		} catch(Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
	}
	
	/**
	 * Treina modelo com base nos par�metros enviados
	 * @throws Exception
	 */
	public void trainModel() throws Exception{
		
		File trainingData = null;
		LabelAwareIterator iterator = null;
		ParagraphVectors paragraphVectors = null;
		TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();		
		System.out.println("Iniciando processo de gera��o do modelo....");
		Timer timer = new Timer();
		
		try {
		
			//Verifica se ja existe modelo no disco, se existir, carrega, sen�o cria
			if(!(new File(this.modelPath + this.modelName  ).exists())) {
				
				//N�o encontrou modelo treinado, realiza treinamento de um novo modelo baseado na base de treinamento definida
				System.out.println("Iniciando gera��o do modelo de acordo com dados de treino definidos em --> " + modelPath); 
									
				//Efetuando leitura da base de dados a ser utilizada para treinamento			
				trainingData = new File(this.trainindData);
				
				/**
				 * Criando um iterator para nosso dataset a ser utilizado no treinamento.
				 * Lembre-se que os dados de treinamento devem ser nomeados exatamente como ocorre aqui em :
				 * "/poc-dl4j-nlp/resources/bases".
				 * Temos uma pasta para cada marca de base, cada uma com sua respectiva base de treinamento 
				 **/
			    iterator = new FileLabelAwareIterator.Builder()
			    		.addSourceFolder(trainingData)
			    		.build();
				
			    /**
			     * Um tokenizer � respons�vel por efetuar um pre-tratamento nos dados de treinamento.
			     * Neste caso estamos deixando somente strings contendo letras e\ou n�meros
			     **/
			    tokenizerFactory.setTokenPreProcessor(new ProductCommonProcessor());
			    			    		        
			    /**
			     * Definindo o modelo aser utilizado no treinamento
			     **/
			    System.out.println("Definindo modelo de rede neural utilizado no treinamento do modelo...");
			    paragraphVectors = new ParagraphVectors.Builder()
			              .learningRate(0.025)//Tamanho da etapa para cada atualiza��o dos coeficientes utilizados na rede neural
			              .minLearningRate(0.001)//� o minimo da taxa de aprendizado. A taxa de aprendizado decai � medida que o n�mero de palavras que voc� treina diminui. Se a taxa de aprendizado diminuir muito, o aprendizado da rede n�o ser� mais eficiente. Isso mant�m os coeficientes em movimento
			              .batchSize(20)//Tamanho do batch individual de processamento
			              .epochs(Integer.valueOf(trainingEpochs))//Numero de �pocas de treinamento
			              .minWordFrequency(1)//N�mero m�nimo de ocorrencias de uma palavra para a mesma compor o modelo
			              .layerSize(1000)//N�mero de dimens�es do vetor de saida
			              .iterate(iterator)//Iterator contendo dados de treinamento			              
			              .trainWordVectors(true)
			              .useAdaGrad(true)
			              .tokenizerFactory(tokenizerFactory)
			              .build();
			    			    			   
			    System.out.println("Iniciando treinamento do modelo....");			    
			    paragraphVectors.fit();
			    System.out.println("Modelo treinado --> " + modelPath + modelName);
			    System.out.println("Tempo decorrido para a gera��o do modelo --> " + timer.toString());
			    
			    System.out.println("Iniciando processo de grava��o do modelo --> " + modelPath + modelName);
			    WordVectorSerializer.writeParagraphVectors(paragraphVectors,  modelPath + modelName);
			    System.out.println("Tempo decorrido ap�s a grava��o do modelo --> " + timer.toString());
			    
			}else {				
				System.out.println("O referido modelo ja existe no diret�rio indicado --> " + ConfigParameters.MODEL_PATH.getValor()+ConfigParameters.MODEL_NAME.getValor());
			}			
		}catch (Exception e) {
			System.out.println("Problemas na gera��o no modelo de dados!");
			throw e;
		}	
	}
}
