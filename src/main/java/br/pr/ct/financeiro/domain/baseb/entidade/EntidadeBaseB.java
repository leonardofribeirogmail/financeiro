package br.pr.ct.financeiro.domain.baseb.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.baseb.dto.EntidadeBaseBDTO;
import lombok.Data;

@Data
@Entity(name = "entidadebaseb")
public class EntidadeBaseB implements Serializable {

	private static final long serialVersionUID = -3113445111267167875L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private Integer idade;
	
	@Column
	private String endereco;
	
	@Column(name = "fonte_renda")
	private String fonteRenda;
	
	@JoinColumn(name = "cpf", referencedColumnName = "cpf", updatable = false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EntidadeBaseA base;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_entidadebase_b")
	private List<ClienteBem> bens;
	
	public static EntidadeBaseB create(final EntidadeBaseBDTO base) {
		final ModelMapper mapper = new ModelMapper();
		return mapper.map(base, EntidadeBaseB.class);
	}
}
