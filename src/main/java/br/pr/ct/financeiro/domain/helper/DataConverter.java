package br.pr.ct.financeiro.domain.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Converter
public class DataConverter implements AttributeConverter<Date, String> {
	
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger LOGGER = LogManager.getLogger(DataConverter.class);

	@Override
	public String convertToDatabaseColumn(Date attribute) {
		if(attribute == null) {
			return null;
		}
		return formatter.format(attribute);
	}

	@Override
	public Date convertToEntityAttribute(String dbData) {
		
		Date retorno = null;
		if(dbData == null || dbData.isEmpty()) {
			return retorno;
		}
		
		try {
			retorno = this.formatter.parse(dbData);
		} catch (ParseException e) {
			LOGGER.info("Erro ao converter a data "+dbData+" em uma data valida.", e);
		}
		
		return retorno;
	}

}
