package br.pr.ct.financeiro.domain.learning.util;

public enum ConfigParameters {

	MODEL_PATH("D:\\Projects\\financeiro\\resources\\"),
	MODEL_NAME("bases.ZIP"),
	MODEL_LABELS_NUMBER("2"),
	TRAIN_DATA_PATH("D:\\Projects\\financeiro\\resources\\bases");
	
	private final String valor;
	
	ConfigParameters(String valorOpcao){
        valor = valorOpcao;
    }
    public String getValor(){
        return valor;
    }
}
