package br.pr.ct.financeiro.domain.exception;

public class BancoDadosException extends RuntimeException {

	private static final long serialVersionUID = -2618677288374693674L;

	public BancoDadosException(final String message) {
		super(message);
	}
	
	public BancoDadosException(final String message, Throwable e) {
		super(message, e);
	}
}
