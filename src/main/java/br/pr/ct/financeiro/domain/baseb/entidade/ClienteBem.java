package br.pr.ct.financeiro.domain.baseb.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "clientebem")
public class ClienteBem implements Serializable {

	private static final long serialVersionUID = 5541663961339967341L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String tipo;
	
	@Column(name = "vr_bem")
	private Double vrBem;
	
	@Column
	private String descricao;
}
