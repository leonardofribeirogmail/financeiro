package br.pr.ct.financeiro.domain.exception;

public class CartaoInvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1042688989893397549L;
	
	public CartaoInvalidoException(final String mensagem) {
		super(mensagem);
	}
	
	public CartaoInvalidoException(final String mensagem, final Throwable exception) {
		super(mensagem, exception);
	}
}
