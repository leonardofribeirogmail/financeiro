package br.pr.ct.financeiro.domain.basec.entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.basec.dto.EntidadeBaseCDTO;
import lombok.Data;

@Data
@Entity(name = "entidadebasec")
@Table(indexes = {@Index(columnList = "cpf, id_ultima_compra")})
public class EntidadeBaseC implements Serializable {

	private static final long serialVersionUID = 856752847658685036L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "cpf", referencedColumnName = "cpf", updatable = false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EntidadeBaseA base;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_ultima_compra")
	private UltimaCompra ultimaCompra;
	
	@JoinColumn(name = "id_ambiente_base_c")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Bureau> bureaus;
	
	@JoinColumn(name = "id_ambiente_base_c")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Movimentacao> movimentacoes;
	
	public static EntidadeBaseC create(EntidadeBaseCDTO base) {
		final ModelMapper mapper = new ModelMapper();
		return mapper.map(base, EntidadeBaseC.class);
	}
}
