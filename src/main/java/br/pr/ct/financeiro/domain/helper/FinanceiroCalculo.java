package br.pr.ct.financeiro.domain.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.pr.ct.financeiro.domain.basea.entidade.Divida;
import br.pr.ct.financeiro.domain.baseb.entidade.ClienteBem;
import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import br.pr.ct.financeiro.domain.basec.entidade.Movimentacao;

/**
 * @author Leonardo
 * Classe apenas como mock de processamento
 */
@Service
public class FinanceiroCalculo {

	@Value("${app.custom.limite.medio}")
	private String limiteMedio;
	
	public Double calcularLimiteDeCredito(final Double limiteSolicitado, 
		final EntidadeBaseB baseB, final EntidadeBaseC baseC) {
		
		final Double renda = baseB.getBase().getRenda();
		final List<Divida> dividas = baseB.getBase().getDividas();
		final List<ClienteBem> bens = baseB.getBens();
		final List<Movimentacao> movimentacoes = baseC != null ? baseC.getMovimentacoes() : new ArrayList<>();
		
		Double limiteDeCreditoMaximo = (renda + (new Double(this.limiteMedio) * 0.2D)) * 0.3D;
		limiteDeCreditoMaximo = this.correcaoComBaseNoLimiteSolicitado(limiteDeCreditoMaximo, limiteSolicitado);
		limiteDeCreditoMaximo = this.correcaoComBaseNasDividas(limiteDeCreditoMaximo, dividas);
		limiteDeCreditoMaximo = this.correcaoComBaseNosBens(limiteDeCreditoMaximo, bens);
		limiteDeCreditoMaximo = this.correcaoComBaseNasMovimentacoes(limiteDeCreditoMaximo, movimentacoes);
		limiteDeCreditoMaximo = this.calculoAprendizadoMaquina(limiteDeCreditoMaximo, baseB.getBase().getCpf());
		return limiteDeCreditoMaximo;
	}
	
	private Double getMultiplicador(final int size) {
		return (1 - Double.valueOf("0.1"+size));
	}
	
	private Double correcaoComBaseNoLimiteSolicitado(Double limiteDeCredito, Double limiteSolicitado) {
		//Correcao com base no limite solicitado
		//Logica nao aplicada
		final Double multiplicador = Double.valueOf("1.0") + limiteSolicitado;
		return limiteDeCredito * multiplicador;
	}
	
	private Double correcaoComBaseNasDividas(Double limiteDeCredito, final List<Divida> dividas) {
		//Correcao de valor com base nas dividas
		//Logica nao aplicada
		return limiteDeCredito * this.getMultiplicador(dividas != null ? dividas.size() : 1);
	}
	
	private Double correcaoComBaseNosBens(Double limiteDeCredito, final List<ClienteBem> bens) {
		//Correcao de valor com base nos bens
		//Logica nao aplicada
		return limiteDeCredito * this.getMultiplicador(bens != null ? bens.size() : 1);
	}
	
	private Double correcaoComBaseNasMovimentacoes(Double limiteDeCredito, final List<Movimentacao> movimentacoes) {
		//Correcao de valor com base nas movimentacoes
		//Logica nao aplicada
		return limiteDeCredito * this.getMultiplicador(movimentacoes != null ? movimentacoes.size() : 1);
	}
	
	private Double calculoAprendizadoMaquina(Double limiteDeCredito, String cpf) {
		//Correcao com base em aprendizado de maquina
		//Logica nao aplicada
		int size = cpf.length();
		return limiteDeCredito * this.getMultiplicador(size);
	}
}
