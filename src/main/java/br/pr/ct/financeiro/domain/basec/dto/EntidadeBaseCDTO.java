package br.pr.ct.financeiro.domain.basec.dto;

import java.io.Serializable;
import java.util.List;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import lombok.Data;

@Data
public class EntidadeBaseCDTO implements Serializable {

	private static final long serialVersionUID = 9135568414531295541L;

	private Long id;
	private EntidadeBaseADTO base;
	private UltimaCompraDTO ultimaCompra;
	private List<BureauDTO> bureaus;
	private List<MovimentacaoDTO> movimentacoes;
	
	public static EntidadeBaseCDTO create(final EntidadeBaseC base) {
		final ModelMapper mapper = new ModelMapper();
		final EntidadeBaseCDTO baseCDTO = mapper.map(base, EntidadeBaseCDTO.class);
		baseCDTO.base = EntidadeBaseADTO.create(baseCDTO.base.getId());
		return baseCDTO;
	}
}
