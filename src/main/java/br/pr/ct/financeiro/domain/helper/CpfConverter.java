package br.pr.ct.financeiro.domain.helper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CpfConverter implements AttributeConverter<String, String> {

	@Override
	public String convertToDatabaseColumn(String attribute) {
		return replaceAttribute(attribute);
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		return deplaceAttribute(dbData);
	}
	
	public static String replace(final String cpf) {
		return replaceAttribute(cpf);
	}
	
	public static String deplace(final String cpf) {
		return deplaceAttribute(cpf);
	}
	
	private static String replaceAttribute(final String attribute) {
		if(attribute!= null && !attribute.isEmpty()) {
        	return attribute.replaceAll("\\.", "").replaceAll("\\-", "");
        }
		
		return null;
	}
	
	private static String deplaceAttribute(final String dbData) {
		String cpf = dbData;
        if (cpf != null && cpf.length() == 11) {
        	cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
        }
        return cpf;
	}
}
