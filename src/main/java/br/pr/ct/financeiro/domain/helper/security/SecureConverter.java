package br.pr.ct.financeiro.domain.helper.security;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.pr.ct.financeiro.domain.exception.SecretConversionException;

@Service
public class SecureConverter {
	
	@Value("${app.custom.security.chave}")
	private String secretKey;
	private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
	
	public String convertToDatabaseColumn(String attribute) {
		try {
			return Base64.encodeBase64String(this.encrypt(this.secretKey, attribute));
		} catch (Exception e) {
		   throw new SecretConversionException(e);
		}
	}
	
	public String convertToEntityAttribute(String attribute) {
		try {
			return this.decrypt(this.secretKey, Base64.decodeBase64(attribute));
		} catch (Exception e) {
		   throw new SecretConversionException(e);
		}
	}
	
	private  byte[] encrypt(String key, String value)
	      throws GeneralSecurityException {
	   byte[] raw = key.getBytes(StandardCharsets.UTF_8);
	   if (raw.length != 16) {
	     throw new IllegalArgumentException("Invalid key size.");
	   }

	   SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
	   Cipher cipher = Cipher.getInstance(ALGORITHM);
	   cipher.init(Cipher.ENCRYPT_MODE, skeySpec,
	       new IvParameterSpec(new byte[16]));
	   return cipher.doFinal(value.getBytes(StandardCharsets.UTF_8));
	}

	private String decrypt(String key, byte[] encrypted)
	      throws GeneralSecurityException {

	   byte[] raw = key.getBytes(StandardCharsets.UTF_8);
	   if (raw.length != 16) {
	     throw new IllegalArgumentException("Invalid key size.");
	   }
	   SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

	   Cipher cipher = Cipher.getInstance(ALGORITHM);
	   cipher.init(Cipher.DECRYPT_MODE, skeySpec,
	       new IvParameterSpec(new byte[16]));
	   byte[] original = cipher.doFinal(encrypted);

	   return new String(original, StandardCharsets.UTF_8);
	}
}
