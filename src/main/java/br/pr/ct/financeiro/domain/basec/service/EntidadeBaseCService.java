package br.pr.ct.financeiro.domain.basec.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.pr.ct.financeiro.domain.basec.dto.EntidadeBaseCDTO;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import br.pr.ct.financeiro.domain.basec.repository.EntidadeBaseCRepository;
import br.pr.ct.financeiro.domain.exception.BancoDadosException;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;

@Service
public class EntidadeBaseCService {

	@Autowired
	private MensagemHelper mensagem;
	
	@Autowired
	private EntidadeBaseCRepository repository;
	
	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	private EntityManager manager;
	
	public List<EntidadeBaseCDTO> getAll(Pageable pageable) {
		return this.repository.findAll(pageable).stream().map(EntidadeBaseCDTO::create).collect(Collectors.toList());
	}
	
	@Transactional
	public EntidadeBaseCDTO inserir(EntidadeBaseC base) {
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		Assert.notNull(base.getBase(), this.mensagem.objetoInvalido);
		Assert.notNull(base.getBase().getId(), this.mensagem.objetoInvalido);
		base = this.manager.merge(base);
		return EntidadeBaseCDTO.create(base);
	}
	
	public EntidadeBaseCDTO update(Long id, EntidadeBaseC base) {
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		final Optional<EntidadeBaseC> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			EntidadeBaseC db = optional.get();
			db.setBureaus(base.getBureaus());
			db.setMovimentacoes(base.getMovimentacoes());
			db.setUltimaCompra(base.getUltimaCompra());
			db = this.repository.save(db);
			return EntidadeBaseCDTO.create(db);
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	}
	
	public EntidadeBaseCDTO getById(Long id) {
		Optional<EntidadeBaseC> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			final EntidadeBaseC base = optional.get();
			return EntidadeBaseCDTO.create(base);
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	}
	
	public void delete(Long id) {
		Assert.notNull(id, this.mensagem.objetoNaoEncontrado);
		final EntidadeBaseC base = this.repository.getOne(id);
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		this.repository.delete(base);
	}
}
