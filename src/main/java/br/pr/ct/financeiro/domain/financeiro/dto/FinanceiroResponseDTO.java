package br.pr.ct.financeiro.domain.financeiro.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class FinanceiroResponseDTO implements Serializable {

	private static final long serialVersionUID = 4470980697631580629L;

	private String cpf;
	private Double limite;
}
