package br.pr.ct.financeiro.domain.basea.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.basea.repository.EntidadeBaseARepository;
import br.pr.ct.financeiro.domain.exception.BancoDadosException;
import br.pr.ct.financeiro.domain.helper.CpfConverter;
import br.pr.ct.financeiro.domain.helper.MensagemHelper;

@Service
public class EntidadeBaseAService {
	
	@Autowired
	private MensagemHelper mensagem;

	@Autowired
	private EntidadeBaseARepository repository;
	
	public EntidadeBaseADTO inserir(EntidadeBaseA base) {
		Assert.notNull(base, this.mensagem.objetoNaoEncontrado);
		base = this.repository.save(base);
		return EntidadeBaseADTO.create(base);
	}
	
	public List<EntidadeBaseADTO> getAll(Pageable pageable) {
		return this.repository.findAll(pageable).stream().map(EntidadeBaseADTO::create).collect(Collectors.toList());
	}
	
	public EntidadeBaseADTO getById(Long id) {
		Assert.notNull(id, this.mensagem.objetoNaoEncontrado);
		final Optional<EntidadeBaseA> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			return EntidadeBaseADTO.create(optional.get());
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	}

	public EntidadeBaseADTO update(Long id, EntidadeBaseA base) {
		Assert.notNull(id, this.mensagem.objetoNaoEncontrado);
		final Optional<EntidadeBaseA> optional = this.repository.findById(id);
		if(optional.isPresent()) {
			EntidadeBaseA db = optional.get();
			db.setCpf(base.getCpf());
			db.setEndereco(base.getEndereco());
			db.setNome(base.getNome());
			db.setDividas(base.getDividas());
			db = this.repository.save(db);
			return EntidadeBaseADTO.create(db);
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado); 
		}
	}

	public EntidadeBaseADTO getByCpf(final String cpf) {
		Assert.notNull(cpf, this.mensagem.objetoNaoEncontrado);
		final Optional<EntidadeBaseA> optional = this.repository.findByCpf(CpfConverter.replace(cpf));
		if(optional.isPresent()) {
			return EntidadeBaseADTO.create(optional.get());
		} else {
			throw new BancoDadosException(this.mensagem.objetoNaoEncontrado);
		}
	} 
}
