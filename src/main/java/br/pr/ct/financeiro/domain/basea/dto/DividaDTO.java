package br.pr.ct.financeiro.domain.basea.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DividaDTO implements Serializable {

	private static final long serialVersionUID = -5083714980850947582L;

	private Long id;
	private Double vrDivida;
}
