package br.pr.ct.financeiro.domain.basec.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class BureauDTO implements Serializable {

	private static final long serialVersionUID = -5243425044782515474L;

	private Long id;
	private Date dtConsulta;
	private String nomeBureau;
}
