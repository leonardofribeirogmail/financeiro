package br.pr.ct.financeiro.domain.baseb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;

public interface EntidadeBaseBRepository extends JpaRepository<EntidadeBaseB, Long> {

}
