package br.pr.ct.financeiro.domain.baseb.dto;

import java.io.Serializable;
import java.util.List;

import org.modelmapper.ModelMapper;

import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import lombok.Data;

@Data
public class EntidadeBaseBDTO implements Serializable {

	private static final long serialVersionUID = -1611725891551906453L;

	private Long id;
	private Integer idade;
	private String endereco;
	private String fonteRenda;
	private EntidadeBaseADTO base;
	
	private List<ClienteBemDTO> bens;
	
	public static EntidadeBaseBDTO create(EntidadeBaseB base) {
		final ModelMapper mapper = new ModelMapper();
		final EntidadeBaseBDTO baseBDTO = mapper.map(base, EntidadeBaseBDTO.class);
		baseBDTO.base = EntidadeBaseADTO.create(baseBDTO.base.getId());
		return baseBDTO;
	}
}
