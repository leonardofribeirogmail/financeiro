package br.pr.ct.financeiro.domain.basec.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;

public interface EntidadeBaseCRepository extends JpaRepository<EntidadeBaseC, Long> {

}
