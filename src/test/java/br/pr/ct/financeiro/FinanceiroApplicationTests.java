package br.pr.ct.financeiro;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.pr.ct.financeiro.domain.basea.dto.DividaDTO;
import br.pr.ct.financeiro.domain.basea.dto.EntidadeBaseADTO;
import br.pr.ct.financeiro.domain.basea.entidade.EntidadeBaseA;
import br.pr.ct.financeiro.domain.basea.service.EntidadeBaseAService;
import br.pr.ct.financeiro.domain.baseb.dto.ClienteBemDTO;
import br.pr.ct.financeiro.domain.baseb.dto.EntidadeBaseBDTO;
import br.pr.ct.financeiro.domain.baseb.entidade.EntidadeBaseB;
import br.pr.ct.financeiro.domain.baseb.service.EntidadeBaseBService;
import br.pr.ct.financeiro.domain.basec.dto.BureauDTO;
import br.pr.ct.financeiro.domain.basec.dto.EntidadeBaseCDTO;
import br.pr.ct.financeiro.domain.basec.dto.MovimentacaoDTO;
import br.pr.ct.financeiro.domain.basec.dto.UltimaCompraDTO;
import br.pr.ct.financeiro.domain.basec.entidade.EntidadeBaseC;
import br.pr.ct.financeiro.domain.basec.service.EntidadeBaseCService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
class FinanceiroApplicationTests {
	
	@Autowired
	private EntidadeBaseAService serviceA;
	
	@Autowired
	private EntidadeBaseBService serviceB;
	
	@Autowired
	private EntidadeBaseCService serviceC;
	
	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	private EntityManager manager;
	
	private static final Random RANDOM = new Random();
	
	@Test
	@Transactional @Disabled
	public void testarQuery() {
		final String cpf = "00186202191";
		/*final CriteriaBuilder criteria = this.manager.getCriteriaBuilder();
		CriteriaQuery<EntidadeBaseB> criteriaBaseB = criteria.createQuery(EntidadeBaseB.class);
		Root<EntidadeBaseB> rootBaseB = criteriaBaseB.from(EntidadeBaseB.class);
		criteriaBaseB.distinct(true).where(criteria.equal(rootBaseB.get("base").get("cpf"), "00186202191"));
		
		final EntidadeBaseB entidadeB = this.manager.createQuery(criteriaBaseB).getSingleResult();*/
		final StringBuilder query = new StringBuilder();
		query.append("select baseB from entidadebaseb as baseB join baseB.base as baseA ");
		query.append("where baseA.cpf = '"+cpf+"'");
		TypedQuery<EntidadeBaseB> typedQuery = this.manager.createQuery(query.toString(), EntidadeBaseB.class);
		typedQuery.setMaxResults(1);
		EntidadeBaseB baseB = typedQuery.getSingleResult();
		System.out.println(baseB.getBase().getNome());
	}
	
	@Test @Disabled
	public void incluirBaseA() {
		
		int quantidade = 0;
		
		for(int i=0;i<50;i++) {
			try {
				final String doisUltimosNumeros = String.valueOf(RANDOM.nextInt((99 - 10) + 1) + 10);
				EntidadeBaseADTO baseA = new EntidadeBaseADTO();
				final List<DividaDTO> dividas = new ArrayList<>();
				
				DividaDTO divida = new DividaDTO();
				divida.setVrDivida(this.getDouble());
				dividas.add(divida);
				
				divida = new DividaDTO();
				divida.setVrDivida(this.getDouble());
				dividas.add(divida);
				
				divida = new DividaDTO();
				divida.setVrDivida(this.getDouble());
				dividas.add(divida);
				
				divida = new DividaDTO();
				divida.setVrDivida(this.getDouble());
				dividas.add(divida);
				
				divida = new DividaDTO();
				divida.setVrDivida(this.getDouble());
				dividas.add(divida);
				
				baseA.setDividas(dividas);
				
				baseA.setRenda(this.getDouble());
				baseA.setEndereco("Meu endereço pessoal "+RANDOM.nextLong());
				baseA.setNome("Leonardo Fernandes "+RANDOM.nextLong());
				baseA.setCpf("001.862.021-"+doisUltimosNumeros);
				baseA = this.serviceA.inserir(EntidadeBaseA.create(baseA));
				this.incluirBaseB(baseA);
				this.incluirBaseC(baseA);
				
				if(baseA != null) quantidade++;
			} catch(Exception e) {
				log.error("Não foi possível criar o index "+i+". "+e.getLocalizedMessage());
			}
		}
		
		System.out.println("Quantidade inserida: "+quantidade);
		assertTrue(true);
	}
	
	@Test @Disabled
	public void testarBaseB() {
		EntidadeBaseADTO base = this.serviceA.getById(1L);
		this.incluirBaseB(base);
		assertTrue(true);
	}
	
	private void incluirBaseB(EntidadeBaseADTO baseA) {
		EntidadeBaseBDTO baseB = new EntidadeBaseBDTO();
		List<ClienteBemDTO> bens = new ArrayList<>();
		
		ClienteBemDTO clienteBem = new ClienteBemDTO();
		clienteBem.setVrBem(10000D);
		clienteBem.setDescricao("meu imóvel não tão caro assim "+RANDOM.nextLong());
		clienteBem.setTipo("barraco");
		
		bens.add(clienteBem);
		
		clienteBem = new ClienteBemDTO();
		clienteBem.setVrBem(20000D);
		clienteBem.setDescricao("meu imóvel um pouco mais caro, mas ainda barato "+RANDOM.nextLong());
		clienteBem.setTipo("barraco");
		
		bens.add(clienteBem);
		
		clienteBem = new ClienteBemDTO();
		clienteBem.setVrBem(100000D);
		clienteBem.setDescricao("meu imóvel mais carinho");
		clienteBem.setTipo("casa");
		
		bens.add(clienteBem);
		
		clienteBem = new ClienteBemDTO();
		clienteBem.setVrBem(2500100D);
		clienteBem.setDescricao("minha humilde cobertura");
		clienteBem.setTipo("apartamento");
		
		bens.add(clienteBem);
		baseB.setBens(bens);
		baseB.setBase(baseA);
		baseB.setEndereco("algum endereço qualquer que não tô ligado");
		baseB.setFonteRenda("autonômo");
		baseB.setIdade(36);
		
		System.out.println("Salvando: "+baseB.toString());
		
		baseB = this.serviceB.inserir(EntidadeBaseB.create(baseB));
		assertTrue(baseB != null);
	}
	
	private void incluirBaseC(EntidadeBaseADTO baseA) {
		EntidadeBaseCDTO baseC = new EntidadeBaseCDTO();
		baseC.setBase(baseA);
		
		final List<BureauDTO> bureaus = new ArrayList<>();
		BureauDTO bureau = new BureauDTO();
		bureau.setNomeBureau("ProtecaoCreditoA");
		
		bureau.setDtConsulta(this.getCalendar(-1).getTime());
		
		bureaus.add(bureau);
		
		bureau = new BureauDTO();
		bureau.setNomeBureau("ProtecaoCreditoB");
		bureau.setDtConsulta(this.getCalendar(-2).getTime());
		
		bureaus.add(bureau);
		
		bureau = new BureauDTO();
		bureau.setNomeBureau("ProtecaoCreditoC");
		bureau.setDtConsulta(this.getCalendar(-3).getTime());
		
		bureaus.add(bureau);
		baseC.setBureaus(bureaus);
		
		final List<MovimentacaoDTO> movimentacoes = new ArrayList<>();
		MovimentacaoDTO movimentacao = new MovimentacaoDTO();
		movimentacao.setDtMovimento(this.getCalendar(-10));
		movimentacao.setVrMovimento(this.getDouble());
		movimentacoes.add(movimentacao);
		
		movimentacao = new MovimentacaoDTO();
		movimentacao.setDtMovimento(this.getCalendar(-8));
		movimentacao.setVrMovimento(this.getDouble());
		movimentacoes.add(movimentacao);
		
		movimentacao = new MovimentacaoDTO();
		movimentacao.setDtMovimento(this.getCalendar(-5));
		movimentacao.setVrMovimento(this.getDouble());
		movimentacoes.add(movimentacao);
		
		baseC.setMovimentacoes(movimentacoes);
		
		UltimaCompraDTO ultimaCompra = new UltimaCompraDTO();
		ultimaCompra.setCartao(this.getNumeroCartao());
		ultimaCompra.setDtCompra(this.getCalendar(0));
		ultimaCompra.setVrCompra(this.getDouble());
		baseC.setUltimaCompra(ultimaCompra);
		
		System.out.println("Inserindo "+baseC.toString());
		
		baseC = this.serviceC.inserir(EntidadeBaseC.create(baseC));
		assertTrue(baseC != null);
	}
	
	private Calendar getCalendar(int days) {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE) + days);
		return calendar;
	}
	
	private String getNumeroCartao() {
		List<String> numeros = Arrays.asList(new String[] {
			"9874676249462530",
			"8562486413818056",
			"5137441596545661",
			"3669750467014037",
			"1879218224104605",
			"2588698421373401",
			"1315522545832348",
			"2104970657487943",
			"5901633766496859",
			"1203361423513236"
		});
		
		return numeros.get(RANDOM.nextInt(numeros.size()));
	}
	
	private Double getDouble() {
		final Double rangeMin = 10000D;
		final Double rangeMax = 1000000D;
		return rangeMin + (rangeMax - rangeMin) * RANDOM.nextDouble();
	}
}
