package br.pr.ct.financeiro.main;

import br.pr.ct.financeiro.domain.learning.model.DeepModelUtils;
import br.pr.ct.financeiro.domain.learning.util.ConfigParameters;

public class TesteMain {

	public static void main(String[] args) throws Exception {
		DeepModelUtils models = new DeepModelUtils(
			ConfigParameters.MODEL_PATH.getValor(), 
			ConfigParameters.MODEL_NAME.getValor(), 
			ConfigParameters.TRAIN_DATA_PATH.getValor(), "10");
		models.trainModel();
	}
}
